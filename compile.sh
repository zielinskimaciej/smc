#!/bin/bash

if [ "$1" = "it" ]
then
    echo "Execute integration tests..."
    ./gradlew clean build integrationTest
elif [ "$1" = "at" ]
then
    echo "Execute all tests (unit, integration and functional tests on chrome browser)..."
    ./gradlew clean build integrationTest functionalTest
elif [ "$1" = "sonar" ]
   then
    ./gradlew clean test sonar
else
    echo "Build project and run junits..."
    ./gradlew clean build
fi
