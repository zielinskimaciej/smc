# \#\#\# **Software Mill Challenge** \#\#\#

## \# **HowTo**

### 1. Compile

>You have to have Java 8 installed on local environment !!!

- **./compile.sh** - build war and run only unit tests
- **./compile.sh it** - build war, run unit and integration tests
- **./compile.sh at** - build war, run acceptance tests (unit, integration and functional tests on chrome)
- **./compile.sh sonar** - analyze project by sonar - there should be running sonar on http://localhost:9000

### 2. Deploy
- **./deploy.sh** - run and deploy application on embedded Jetty
- **./deploy.sh tomcat** - copy war into $CATALINA_HOME/webapps folder

## \# **Answers**

### 1. Frontend [Knockout.js + Java-Script + HTML]

**Main goal**: make client application in separation to backend

Before I haven't have any experience with Knockout.js (and very basic in js). I choose this framework after little
research. This framework convinced me because:

- it's light
- has pretty good documentation
- automatically keeps model updated - very useful feature

### 2. Backend [Java 8, Spark Web Framework, Guice, Lombok, Bean Validation], Test: [Groovy, Spock, REST-Assured, Geb]

First of all, this was my first project in Java 8, so I was very happy, that I could use lambda's :)

- **Spark** - I was looking for light framework which will be similar to scalatra. I saw scalatra framework on JUG presentation prepared by Michal Ostruszka ;D
- **Guice** - I wanted to use a framework which will handle Dependency Injection. I used Guice, because it is written specially for that, supports JSR 330 
and it has a lot of extensions - I used for example for JMX and Bean Validation. Probably, in the future, I will change Guice to Spring, 
when I will add Spring-Data and transactions to service layer (which should be very easy, because Guice supports JSR 330).
- **Lombok** - I wanted to reduce boilerplate code like builders, setters, getters, initialize loggers, etc
- **Spock** - I wanted to write tests in more readable way. Spock allowed me to that, because I write tests in Groovy. 
Additionally, I do not have to add any additional libraries for mocking or for parametrize tests. It is consistent with BDD approach.
- **REST-Assured** - I wanted to use framework which will help me to execute and validate REST API in integration tests.
- **Geb** - I wanted to use framework which will help me to write functional web tests in Groovy and will support Spock.
- **Bean Validation** - I used this framework to validate model's data, to prevent wrong data in DataSource. 
It is part of Java Specification (JSR 303), simple, clean, easy to use and test that why I chose this library.

From above list I have used only Spock before :)

### 3. What are my suggestions about persistence layer?

As I've wrote before, I would like to add Spring-Data and some transactional database. For tests I would like to add database in memory. 
I was trying to prepare all repositories in consistence with Spring-Data, so I shouldn't have huge problem to improved this layer.

### 4. How much time I was doing this application?

It's very hard question. It took me 2 weeks, but I haven't worked every day.
There were days when I have been working for 1 hour, but there were also days when I have been working for 4/5 hours per day.

3 days I have been implementing improvements after review - more or less 10 hours.

### 5. How would you modify application, when it would turn out that lists of products and availability should be taken from warehouse's server via REST API ones a day

I would like to add external module, specially for batch processes. I would use probably spring-batch for that, 
but for sure I would have to make a research before I start implementation, to choose correct framework, because there are some 
alternatives like EasyBatch, JSR-352 (available on application servers), etc.

Then I would like to implement schedule job, which will be executed ones a day at specific time (CRON) according with requirements.
Because my DataSource in available from outside (via JMX), I can easily apply retrieved data to DataSource from batch process. 
even when batch application will be running on different JVM.

Probably I would use RestTemplate (Spring) to fetch data from warehouse REST API, 
but same as previously I would have to make research, because this depends on details requirements.

## PS. I hope that there will be a lot of comments :)
What I would like to change - write code in DDD way, but I haven't have enough knowledge and experience in this approach.
