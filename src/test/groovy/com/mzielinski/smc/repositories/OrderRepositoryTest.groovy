package com.mzielinski.smc.repositories

import com.mzielinski.smc.datasource.DataSourceMBean
import com.mzielinski.smc.model.entities.Order
import spock.lang.Specification

class OrderRepositoryTest extends Specification {

    OrderRepository objectUnderTest = new OrderRepository()

    DataSourceMBean dataSourceMock = Mock(DataSourceMBean)

    def setup() {
        objectUnderTest.dataSource = dataSourceMock
    }

    def "should create order in data source"() {
        given:
        Order order = Order.builder().lines([])build()

        when:
        objectUnderTest.save(order)

        then:
        1 * dataSourceMock.insertOrder(order)
    }

}
