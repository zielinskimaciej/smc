package com.mzielinski.smc.repositories

import com.mzielinski.smc.datasource.DataSourceMBean
import com.mzielinski.smc.exceptions.UnknownProductException
import com.mzielinski.smc.model.entities.Product
import spock.lang.FailsWith
import spock.lang.Specification
import spock.lang.Unroll

class ProductRepositoryTest extends Specification {

    ProductRepository objectUnderTest = new ProductRepository()

    DataSourceMBean dataSourceMock = Mock(DataSourceMBean)

    def setup() {
        objectUnderTest.dataSource = dataSourceMock
    }

    def "should return all products fetched from data source"() {
        given:
        def products = [Product.builder().build()]
        dataSourceMock.selectProducts() >> products

        when:
        Iterable<Product> allProducts = objectUnderTest.findAll()

        then:
        allProducts == products
    }

    def "should find product for given id if exists in data source"() {
        given:
        Product product = Product.builder().build()
        dataSourceMock.selectProduct(product.id) >> product

        when:
        Product productById = objectUnderTest.findOne(product.id)

        then:
        productById == product
    }

    @Unroll
    @FailsWith(value = IllegalArgumentException)
    def "should throw IllegalArgumentException when user want to find product and id is [#id]"() {
        expect:
        objectUnderTest.findOne(id)

        where:
        id << [-1, 0]
    }


    def "should update product in data source"() {
        given:
        Product product = Product.builder().build()

        when:
        objectUnderTest.save(product)

        then:
        1 * dataSourceMock.updateProduct(product)
    }

    @FailsWith(value = UnknownProductException)
    def "should throw UnknownProductException when product with given id does not exists in data source"() {
        given:
        long product = 1L
        dataSourceMock.selectProduct(product) >> null

        expect:
        objectUnderTest.findOne(product)
    }

}
