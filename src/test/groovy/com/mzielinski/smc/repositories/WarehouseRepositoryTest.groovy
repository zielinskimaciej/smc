package com.mzielinski.smc.repositories

import com.mzielinski.smc.datasource.DataSourceMBean
import com.mzielinski.smc.model.entities.WarehouseItem
import spock.lang.FailsWith
import spock.lang.Specification
import spock.lang.Unroll

class WarehouseRepositoryTest extends Specification {

    WarehouseRepository objectUnderTest = new WarehouseRepository()

    DataSourceMBean dataSourceMock = Mock(DataSourceMBean)

    def setup() {
        objectUnderTest.dataSource = dataSourceMock
    }

    def "should find warehouse item for given product id if exists in data source"() {
        given:
        WarehouseItem item = WarehouseItem.builder().product(1L).quantity(10).build()
        dataSourceMock.selectWarehouseByProduct(item.product) >> item

        when:
        WarehouseItem warehouseItem = objectUnderTest.findByProduct(item.product)

        then:
        warehouseItem == item
    }

    @Unroll
    @FailsWith(value = IllegalArgumentException)
    def "should throw IllegalArgumentException when user want to find warehouse item for invalid product id [#id]"() {
        expect:
        objectUnderTest.findByProduct(id)

        where:
        id << [-1, 0]
    }


    def "should update warehouse item in data source"() {
        given:
        WarehouseItem item = WarehouseItem.builder().product(1L).quantity(10).build()

        when:
        objectUnderTest.save(item)

        then:
        1 * dataSourceMock.updateWarehouse(item)
    }

}
