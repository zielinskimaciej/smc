package com.mzielinski.smc.datasource

import com.mzielinski.smc.model.entities.WarehouseItem
import spock.lang.FailsWith
import spock.lang.Specification
import spock.lang.Unroll

import static org.assertj.core.api.Assertions.assertThat

class WarehouseDataSourceTest extends Specification {

    private DataSource objectUnderTest

    def setup() {
        objectUnderTest = new DataSource();
    }

    def "should save warehouse item"() {
        given:
        def item = WarehouseItem.builder().build()

        when:
        def product = objectUnderTest.insertWarehouse(item)

        then:
        product.id > 0
        assertThat(objectUnderTest.selectWarehouse()).containsExactly(item)
    }

    def "should find warehouse item with given product id"() {
        given:
        def item = objectUnderTest.insertWarehouse(WarehouseItem.builder().product(1L).build())

        expect:
        objectUnderTest.selectWarehouseByProduct(item.product) == item
    }

    @Unroll
    @FailsWith(IllegalArgumentException)
    def "should throw IllegalArgumentException where user try to select warehouse item with invalid product id [#id]"() {
        expect:
        objectUnderTest.selectWarehouseByProduct(product)

        where:
        product << [-1, 0]
    }


    def "should return null when warehouse item with given product id not exist"() {
        expect:
        objectUnderTest.selectWarehouseByProduct(123L) == null
    }

    def "should find all saved warehouse items"() {
        given:
        def item1 = objectUnderTest.insertWarehouse(WarehouseItem.builder().build())
        def item2 = objectUnderTest.insertWarehouse(WarehouseItem.builder().build())
        def item3 = objectUnderTest.insertWarehouse(WarehouseItem.builder().build())

        when:
        def warehouse = objectUnderTest.selectWarehouse()

        then:
        assertThat(warehouse).containsExactly(item1, item2, item3)
    }

    def "should update saved warehouse item"() {
        given:
        def item = objectUnderTest.insertWarehouse(WarehouseItem.builder().product(1).quantity(10).build())
        def updatedItem = WarehouseItem.builder()
                .id(item.id)
                .createDate(item.createDate)
                .product(item.product)
                .quantity(5)
                .build();

        when:
        def savedItem = objectUnderTest.updateWarehouse(updatedItem)

        then:
        savedItem.quantity == updatedItem.quantity
        objectUnderTest.selectWarehouseByProduct(updatedItem.product).quantity == updatedItem.quantity
    }

}
