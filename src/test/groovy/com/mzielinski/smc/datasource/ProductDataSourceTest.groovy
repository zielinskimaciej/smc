package com.mzielinski.smc.datasource

import com.mzielinski.smc.model.entities.Product
import spock.lang.FailsWith
import spock.lang.Specification
import spock.lang.Unroll

import static org.assertj.core.api.Assertions.assertThat

class ProductDataSourceTest extends Specification {

    private DataSource objectUnderTest

    def setup() {
        objectUnderTest = new DataSource();
    }

    def "should save product"() {
        given:
        def productStub = Product.builder().build()

        when:
        def product = objectUnderTest.insertProduct(productStub)

        then:
        product.id > 0
        assertThat(objectUnderTest.selectProducts()).containsExactly(product)
    }

    def "should find product with given id"() {
        given:
        def productStub = objectUnderTest.insertProduct(Product.builder().build())

        expect:
        objectUnderTest.selectProduct(productStub.id) == productStub
    }

    @Unroll
    @FailsWith(IllegalArgumentException)
    def "should throw IllegalArgumentException where user try to select product with invalid id [#id]"() {
        expect:
        objectUnderTest.selectProduct(id)

        where:
        id << [-1, 0]
    }

    def "should return null when product with given id not exist"() {
        expect:
        objectUnderTest.selectProduct(123L) == null
    }

    def "should find all saved products"() {
        given:
        def product1 = objectUnderTest.insertProduct(Product.builder().build())
        def product2 = objectUnderTest.insertProduct(Product.builder().build())
        def product3 = objectUnderTest.insertProduct(Product.builder().build())

        when:
        def products = objectUnderTest.selectProducts()

        then:
        assertThat(products).containsExactly(product1, product2, product3)
    }

    def "should update saved product"() {
        given:
        def product = objectUnderTest.insertProduct(Product.builder().name("name").build())
        def updatedProduct = Product.copy(product).name("updatedName").build();

        when:
        def savedProduct = objectUnderTest.updateProduct(updatedProduct)

        then:
        savedProduct.name == updatedProduct.name
        objectUnderTest.selectProduct(product.id).name == updatedProduct.name
    }

}
