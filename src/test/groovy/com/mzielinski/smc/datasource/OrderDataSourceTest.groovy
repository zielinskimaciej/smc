package com.mzielinski.smc.datasource

import com.mzielinski.smc.model.entities.Order
import com.mzielinski.smc.model.entities.OrderLine
import com.mzielinski.smc.model.entities.Product
import spock.lang.Specification

import static org.assertj.core.api.Assertions.assertThat

class OrderDataSourceTest extends Specification {

    private DataSource objectUnderTest

    def setup() {
        objectUnderTest = new DataSource();
    }

    def "should save order with all order lines"() {
        given:
        def productStub = Product.builder().build()
        def lineStub = OrderLine.builder().product(productStub.id).quantity(10).subtotal(BigDecimal.TEN).build()
        def orderStub = Order.builder().lines([lineStub]).build()

        when:
        def order = objectUnderTest.insertOrder(orderStub)

        then:
        order.id > 0
        order.lines.each {
            assert it.product == productStub.id
            assert it.id > 0
        }

        and:
        assertThat(objectUnderTest.selectOrders()).containsExactly(order)
    }

    def "should find all saved orders"() {
        given:
        def order1 = objectUnderTest.insertOrder(Order.builder().lines([]).build())
        def order2 = objectUnderTest.insertOrder(Order.builder().lines([]).build())

        when:
        def orders = objectUnderTest.selectOrders()

        then:
        assertThat(orders).containsExactly(order1, order2)
    }

}
