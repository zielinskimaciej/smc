package com.mzielinski.smc.datasource

import com.mzielinski.smc.exceptions.SmcAdministrationException
import com.mzielinski.smc.model.entities.Order
import com.mzielinski.smc.model.entities.Product
import spock.lang.FailsWith
import spock.lang.Specification

import javax.management.MBeanServer
import javax.management.NotCompliantMBeanException
import javax.management.ObjectName

import static org.assertj.core.api.Assertions.assertThat

class DataSourceTest extends Specification {

    private DataSource objectUnderTest

    def setup() {
        objectUnderTest = new DataSource();
    }

    def "should clear all saved data"() {
        given:
        objectUnderTest.insertProduct(Product.builder().build())
        objectUnderTest.insertOrder(Order.builder().lines([]).build())

        when:
        objectUnderTest.clear();

        then:
        objectUnderTest.selectProducts().size() == 0
        objectUnderTest.selectOrders().size() == 0
    }

    def "should register MBean"() {
        given:
        def serverMock = Mock(MBeanServer)

        when:
        objectUnderTest.register(serverMock)

        then:
        1 * serverMock.registerMBean(objectUnderTest, new ObjectName(DataSource.DATA_SOURCE_MBEAN_NAME))
    }

    @FailsWith(value = SmcAdministrationException)
    def "should throw SmcAdministrationException when register will fail"() {
        given:
        def serverMock = Mock(MBeanServer)
        serverMock.registerMBean(objectUnderTest, _ as ObjectName) >> { throw new NotCompliantMBeanException() }

        expect:
        objectUnderTest.register(serverMock)
    }

    def "should create two products and warehouse items as a init data"() {
        when:
        objectUnderTest.createInitData();

        then:
        assertThat(objectUnderTest.selectProducts()).hasSize(2)
        assertThat(objectUnderTest.selectWarehouse()).hasSize(2)
        assertThat(objectUnderTest.selectOrders()).hasSize(0)
    }

}
