package com.mzielinski.smc.servlet.enpoints

import com.jayway.restassured.http.ContentType
import com.jayway.restassured.specification.RequestSpecification
import com.mzielinski.smc.model.entities.Order
import com.mzielinski.smc.model.entities.OrderLine
import com.mzielinski.smc.model.entities.Product
import com.mzielinski.smc.model.entities.WarehouseItem
import com.mzielinski.smc.utils.BaseITSpec

import static com.jayway.restassured.RestAssured.given
import static com.mzielinski.smc.enums.ProductType.TYPE1
import static org.hamcrest.CoreMatchers.equalTo
import static org.hamcrest.CoreMatchers.notNullValue

class OrderEndpointITSpec extends BaseITSpec {

    RequestSpecification requestSpec

    def setup() {
        dataSourceMBean.clear();
        requestSpec = given().port(9393)
    }

    def "should create order and update product for give json"() {
        given:
        Product product = dataSourceMBean.insertProduct(Product.builder().price(1.5).name("name").type(TYPE1).build())
        dataSourceMBean.insertWarehouse(WarehouseItem.builder().quantity(211).product(product.id).build())
        String orderJson = """{"totalPrice":166.50,"lines":[{"product":$product.id,"quantity":111,"subtotal":166.50}]}"""
        requestSpec = requestSpec.given().body(orderJson)

        when:
        requestSpec
        .when()
            .post("/test/order")
        .then()
            .assertThat()
            .body("status", equalTo("SUCCESS"))
            .body("messages[0].cssClass", equalTo("alert-success"))
            .body("messages[0].data", notNullValue())

        then:
        dataSourceMBean.selectWarehouseByProduct(product.id).quantity == 100

        and:
        dataSourceMBean.selectOrders().each { Order order ->
            assert order.totalPrice == 166.50
            order.lines.each { OrderLine line ->
                assert line.product == product.id
                assert line.quantity == 111
                assert line.subtotal == 166.5
            }
        }
    }

    def "should return error message when user try to buy more then available number of items"() {
        given:
        Product product = dataSourceMBean.insertProduct(Product.builder().build())
        dataSourceMBean.insertWarehouse(WarehouseItem.builder().quantity(10).product(product.id).build())
        String orderJson = """{"lines":[{"product":$product.id,"quantity":20}]}"""
        requestSpec = requestSpec.given().body(orderJson)

        expect:
        requestSpec
        .when()
            .post("/test/order")
        .then()
            .assertThat()
            .contentType(ContentType.JSON)
            .body("status", equalTo("WARNING"))
            .body("messages[0].cssClass", equalTo("alert-warning"))
            .body("messages[0].data", notNullValue())
    }

}
