package com.mzielinski.smc.servlet.enpoints

import com.jayway.restassured.http.ContentType
import com.jayway.restassured.specification.RequestSpecification
import com.mzielinski.smc.model.entities.Product
import com.mzielinski.smc.model.entities.WarehouseItem
import com.mzielinski.smc.utils.BaseITSpec
import org.hamcrest.CoreMatchers

import static com.jayway.restassured.RestAssured.given
import static com.jayway.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath
import static org.hamcrest.CoreMatchers.equalTo

class ProductEndpointITSpec extends BaseITSpec {

    RequestSpecification requestSpec

    def setup() {
        dataSourceMBean.clear()
        requestSpec = given().port(9393)
    }

    def "should validate fetched products json with schema"() {
        given:
        Product product1 = dataSourceMBean.insertProduct(Product.builder().name("product-1").description("desc").price(BigDecimal.ONE).build())
        dataSourceMBean.insertWarehouse(WarehouseItem.builder().quantity(87).product(product1.id).build())

        Product product2 = dataSourceMBean.insertProduct(Product.builder().name("product-2").description("desc").price(BigDecimal.TEN).build())
        dataSourceMBean.insertWarehouse(WarehouseItem.builder().quantity(1).product(product2.id).build())

        expect:
        requestSpec
        .when()
            .get("/test/products")
        .then()
            .assertThat().body(matchesJsonSchemaInClasspath("schemas/products-schema.json"));
    }

    def "should validate fetched products with expected values"() {
        given:
        Product product = dataSourceMBean.insertProduct(Product.builder().name("item-1").price(new BigDecimal("12.32")).build())
        dataSourceMBean.insertWarehouse(WarehouseItem.builder().quantity(112).product(product.id).build())

        expect:
        requestSpec
        .when()
            .get("/test/products")
        .then()
            .assertThat()
            .contentType(ContentType.JSON)
            .body("status", equalTo("SUCCESS"))
            .body("object[0].id", equalTo((int) product.id))
            .body("object[0].name", equalTo("item-1"))
            .body("object[0].price", CoreMatchers.is(12.32f))
            .body("object[0].quantity", equalTo(112))
    }

}
