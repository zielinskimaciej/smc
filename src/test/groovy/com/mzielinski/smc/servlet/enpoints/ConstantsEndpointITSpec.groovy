package com.mzielinski.smc.servlet.enpoints

import com.jayway.restassured.specification.RequestSpecification
import com.mzielinski.smc.utils.BaseITSpec

import static com.jayway.restassured.RestAssured.given

class ConstantsEndpointITSpec extends BaseITSpec {

    RequestSpecification requestSpec

    def setup() {
        requestSpec = given().port(9393)
    }

    def "should return plain text for given code"() {
        when:
        String constant = requestSpec
                .when()
                    .get("/test/constant/popup.validation.error")
                .andReturn().body().asString()

        then:
        constant == "There are some errors in order. Please fix it and try ones again."
    }

}
