package com.mzielinski.smc.geb

import com.mzielinski.smc.geb.pages.SmcHomePage
import com.mzielinski.smc.geb.pages.SmcSummaryPage
import com.mzielinski.smc.model.entities.Product
import com.mzielinski.smc.model.entities.WarehouseItem

class SmcAcceptanceGebSpec extends BaseGebSpec {

    def setup() {
        given: "prepare init data and go to home page"
        List<Long> products = setUpProduct()
        SmcHomePage.products = products
        to SmcHomePage

        expect: "verify that user is on home page and there is exactly available 20 items"
        at SmcHomePage
        assert orderLine.availableItems == "20"
    }

    def "should make success order"() {
        when: "insert 14 to input on order line and click BUY button"
        orderLine.quantityInput.value(14)
        buyButton.click()

        then: "verify that user is on summary page"
        waitFor {
            at SmcSummaryPage
        }

        when: "click button FINISH"
        finish.button().click()

        then: "verify that user back to home page with success message"
        waitFor {
            at SmcHomePage
        }

        and:
        assert successMessage
        assert orderLine.availableItems == "6"
    }

    def "should back to home after click CANCEL button in summary page"() {
        when: "insert 12 to input on order line and click BUY button"
        orderLine.quantityInput.value(12)
        buyButton.click()

        then: "verify that user is on summary page"
        waitFor {
            at SmcSummaryPage
        }

        when: "click button CANCEL"
        cancel.button().click()

        then: "verify that user back to home page with previous state"
        waitFor {
            at SmcHomePage
        }
        assert orderLine.quantityInput.value() == "12"
        assert orderLine.availableItems == "8"
    }

    def "should show validation error and modal"() {
        when: "insert wrong value to input on order line"
        secondOrderLine.quantityInput.value(9)
        orderLine.quantityInput.value(123456789)

        then:
        waitFor {
            at SmcHomePage
        }

        and: "verify validation message"
        validationMessage.text() == "Please enter a value less than or equal to 20."

        when: "click button FINISH"
        buyButton.click()

        then:
        waitFor {
            at SmcHomePage
        }

        and: "verify existing of warning modal"
        assert modalDialog
    }

    private static List<Long> setUpProduct() {
        dataSourceMBean.clear()
        Product product1 = dataSourceMBean.insertProduct(Product.builder().name("item1").price(BigDecimal.ONE).build())
        dataSourceMBean.insertWarehouse(WarehouseItem.builder().quantity(20).product(product1.id).build())
        Product product2 = dataSourceMBean.insertProduct(Product.builder().name("item2").price(BigDecimal.ONE).build())
        dataSourceMBean.insertWarehouse(WarehouseItem.builder().quantity(20).product(product2.id).build())
        return [product1.id, product2.id]
    }

}
