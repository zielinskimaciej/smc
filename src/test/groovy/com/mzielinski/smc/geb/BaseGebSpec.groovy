package com.mzielinski.smc.geb

import com.mzielinski.smc.datasource.DataSource
import com.mzielinski.smc.utils.GroovyMBeanCreator
import geb.spock.GebReportingSpec

abstract class BaseGebSpec extends GebReportingSpec {

    static GroovyMBean dataSourceMBean;

    def setupSpec() {
        dataSourceMBean = GroovyMBeanCreator.getMBean(DataSource.DATA_SOURCE_MBEAN_NAME)
    }

}
