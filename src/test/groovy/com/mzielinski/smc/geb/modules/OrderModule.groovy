package com.mzielinski.smc.geb.modules

import geb.Module

class OrderModule extends Module {

    def product

    static content = {

        quantityInput(wait: true) {
            $(id: "order_line_$product")
        }

        availableItems(wait: true) {
            $(id: "progress_bar_$product").text()
        }

        clickBuyButton(wait: true) {
            $("button", id: "buyButton");
        }

    }

}
