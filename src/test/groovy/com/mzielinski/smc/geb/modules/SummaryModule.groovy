package com.mzielinski.smc.geb.modules

import com.mzielinski.smc.geb.pages.SmcSummaryPage
import geb.Module

class SummaryModule extends Module {

    def buttonId

    static content = {

        button(wait: true, to: SmcSummaryPage) {
            $("button", id: buttonId)
        }

    }

}
