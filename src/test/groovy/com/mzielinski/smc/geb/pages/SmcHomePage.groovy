package com.mzielinski.smc.geb.pages

import com.mzielinski.smc.geb.modules.OrderModule
import geb.Page

class SmcHomePage extends Page {

    static List<Long> products

    static url = "/test"

    static at = { title == "Software Mill Challenge Online Shop" }

    static content = {

        orderLine {
            module(OrderModule, product: products[0])
        }

        secondOrderLine {
            module(OrderModule, product: products[1])
        }

        validationMessage {
            orderLine.quantityInput.next()
        }

        buyButton {
            $("button", id: "buyButton")
        }

        successMessage {
            $(id: "alert-success")
        }

        modalDialog(wait: true) {
            $(".modal-dialog")
        }

    }

}