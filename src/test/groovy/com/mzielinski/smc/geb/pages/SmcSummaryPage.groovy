package com.mzielinski.smc.geb.pages

import com.mzielinski.smc.geb.modules.SummaryModule
import geb.Page

class SmcSummaryPage extends Page {

    static url = "/test/#summary"

    static at = {
        $("#summaryPanel")
    }

    static content = {

        finish {
            module SummaryModule, buttonId: "finishButton"
        }

        cancel {
            module SummaryModule, buttonId: "cancelButton"
        }

    }

}
