package com.mzielinski.smc.utils

import javax.management.MBeanServerConnection
import javax.management.remote.JMXConnector
import javax.management.remote.JMXConnectorFactory
import javax.management.remote.JMXServiceURL

class GroovyMBeanCreator {

    static final String serverUrl = "service:jmx:rmi:///jndi/rmi://localhost:1099/jmxrmi"

    static GroovyMBean getMBean(String objectName) {
        final JMXServiceURL jMXServiceURL = new JMXServiceURL(serverUrl)
        final JMXConnector connect = JMXConnectorFactory.connect(jMXServiceURL)
        final MBeanServerConnection server = connect.MBeanServerConnection
        return new GroovyMBean(server, objectName)
    }

}
