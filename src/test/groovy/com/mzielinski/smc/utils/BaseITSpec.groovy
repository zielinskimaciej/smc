package com.mzielinski.smc.utils

import com.mzielinski.smc.datasource.DataSource
import spock.lang.Specification

abstract class BaseITSpec extends Specification {

    static GroovyMBean dataSourceMBean;

    def setupSpec() {
        dataSourceMBean = GroovyMBeanCreator.getMBean(DataSource.DATA_SOURCE_MBEAN_NAME)
    }

}
