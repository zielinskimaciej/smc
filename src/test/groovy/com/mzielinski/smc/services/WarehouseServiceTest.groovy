package com.mzielinski.smc.services

import com.mzielinski.smc.model.entities.WarehouseItem
import com.mzielinski.smc.repositories.WarehouseRepository
import spock.lang.FailsWith
import spock.lang.Specification
import spock.lang.Unroll

class WarehouseServiceTest extends Specification {

    WarehouseRepository warehouseRepositoryMock = Mock(WarehouseRepository)

    WarehouseService objectUnderTest = new WarehouseService(warehouseRepositoryMock)

    def "should decrement number of available product items for given order line"() {
        given:
        WarehouseItem item = WarehouseItem.builder().product(1L).quantity(30).build()
        warehouseRepositoryMock.findByProduct(item.product) >> item

        when:
        objectUnderTest.decrementProductQuantity(item.product, 18)

        then:
        1 * warehouseRepositoryMock.save({ it.quantity == 12 } as WarehouseItem)
    }

    @Unroll
    @FailsWith(value = IllegalArgumentException)
    def "should throw IllegalArgumentException when user wants decrement product quantity and id is [#id]"() {
        expect:
        objectUnderTest.decrementProductQuantity(id, 10)

        where:
        id << [-1, 0]
    }

    @Unroll
    @FailsWith(value = IllegalArgumentException)
    def "should throw IllegalArgumentException when user wants decrement product quantity and quantity is [#quantity]"() {
        expect:
        objectUnderTest.decrementProductQuantity(10, quantity)

        where:
        quantity << [-1, 0]
    }

}
