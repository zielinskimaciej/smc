package com.mzielinski.smc.services

import spock.lang.FailsWith
import spock.lang.Specification
import spock.lang.Unroll

class MessagesServiceTest extends Specification {

    MessagesService objectUnderTest = new MessagesService("test-messages")

    def "should find simple message without any params"() {
        when:
        def message = objectUnderTest.getMessage("test.message")

        then:
        message == "test message"
    }

    def "should fill message with params values"() {
        when:
        def message = objectUnderTest.getMessage("test.message.with.params", "param1", "param2")

        then:
        message == "test message with params param1 param2"
    }

    @Unroll
    @FailsWith(value = IllegalArgumentException)
    def "should throw IllegalArgumentException when code is [#code]"() {
        expect:
        objectUnderTest.getMessage(code)

        where:
        code << [null, ""]
    }

}
