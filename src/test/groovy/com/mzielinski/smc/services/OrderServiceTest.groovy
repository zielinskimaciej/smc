package com.mzielinski.smc.services

import com.mzielinski.smc.model.converters.OrderLineConverter
import com.mzielinski.smc.model.dtos.OrderDto
import com.mzielinski.smc.model.dtos.OrderLineDto
import com.mzielinski.smc.model.entities.Order
import com.mzielinski.smc.model.entities.Product
import com.mzielinski.smc.repositories.OrderRepository
import com.mzielinski.smc.repositories.ProductRepository
import spock.lang.Specification

class OrderServiceTest extends Specification {

    final OrderLineDto defaultLineDto = OrderLineDto.builder().product(123).quantity(10).subtotal(BigDecimal.TEN).build()
    final OrderDto defaultOrderDto = OrderDto.builder().totalPrice(BigDecimal.ONE).lines([defaultLineDto]).build()
    final Product defaultProduct = Product.builder().id(defaultLineDto.product).build()

    WarehouseService warehouseServiceMock = Mock(WarehouseService, constructorArgs: [null])
    OrderRepository orderRepositoryMock = Mock(OrderRepository)
    ProductRepository productsRepositoryMock = Mock(ProductRepository)
    OrderLineConverter orderLineConverterStub = new OrderLineConverter(productsRepositoryMock)

    OrderService objectUnderTest = new OrderService(warehouseServiceMock, orderRepositoryMock, orderLineConverterStub)

    def "should create order based on given DTO"() {
        given:
        Order capturedOrder = null
        productsRepositoryMock.findOne(defaultLineDto.product) >> defaultProduct

        when:
        objectUnderTest.processOrder(defaultOrderDto)

        then:
        1 * orderRepositoryMock.save(_ as Order) >> { capturedOrder = it[0] as Order }

        and:
        capturedOrder.totalPrice == defaultOrderDto.totalPrice
        capturedOrder.lines.each {
            assert it.product == defaultLineDto.product
            assert it.quantity == defaultLineDto.quantity
            assert it.subtotal == defaultLineDto.subtotal
        }
    }

    def "should decrement product quantity based on given order"() {
        given:
            productsRepositoryMock.findOne(defaultLineDto.product) >> defaultProduct

        when:
            objectUnderTest.processOrder(defaultOrderDto)

        then:
            1 * warehouseServiceMock.decrementProductQuantity(defaultLineDto.product, defaultLineDto.quantity)
    }

}
