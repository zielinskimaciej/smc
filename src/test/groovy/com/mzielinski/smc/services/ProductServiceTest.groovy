package com.mzielinski.smc.services

import com.mzielinski.smc.model.converters.ProductConverter
import com.mzielinski.smc.model.dtos.ProductDto
import com.mzielinski.smc.model.entities.Product
import com.mzielinski.smc.model.entities.WarehouseItem
import com.mzielinski.smc.repositories.ProductRepository
import com.mzielinski.smc.repositories.WarehouseRepository
import spock.lang.Specification

import java.time.Month

import static java.time.LocalDateTime.of
import static org.assertj.core.api.Assertions.assertThat

class ProductServiceTest extends Specification {

    WarehouseRepository warehouseRepositoryMock = Mock(WarehouseRepository)
    ProductRepository productRepositoryMock = Mock(ProductRepository)
    ProductConverter productConverterStub = new ProductConverter(warehouseRepositoryMock, productRepositoryMock)

    ProductService objectUnderTest = new ProductService(productRepositoryMock, productConverterStub)

    def "should return all products fetched from data source as ProductDto list "() {
        given:
        productRepositoryMock.findAll() >> [
                Product.builder().id(1).name("product-1").price(new BigDecimal("12.00")).build(),
                Product.builder().id(2).name("product-2").price(new BigDecimal("91.12")).build(),
        ]
        warehouseRepositoryMock.findByProduct(1) >> WarehouseItem.builder().quantity(12).build();
        warehouseRepositoryMock.findByProduct(2) >> WarehouseItem.builder().quantity(21).build();

        when:
        List<ProductDto> productsDto = objectUnderTest.findAllProducts()

        then:
        assertThat(productsDto).containsExactly(
                ProductDto.builder().id(1L).name("product-1").price(new BigDecimal("12.00")).quantity(12).build(),
                ProductDto.builder().id(2L).name("product-2").price(new BigDecimal("91.12")).quantity(21).build()
        )
    }

    def "should return sorted productDTO list"() {
        given:
        productRepositoryMock.findAll() >> [
                Product.builder().id(3).createDate(of(2013, Month.FEBRUARY, 4, 10, 10)).build(),
                Product.builder().id(1).createDate(of(2012, Month.MAY, 4, 10, 9)).build(),
                Product.builder().id(4).createDate(of(2014, Month.DECEMBER, 30, 0, 0)).build(),
                Product.builder().id(2).createDate(of(2012, Month.MAY, 15, 0, 0)).build()
        ]
        4 * warehouseRepositoryMock.findByProduct(_) >> WarehouseItem.builder().build();

        when:
        List<ProductDto> productsDto = objectUnderTest.findAllProducts()

        then:
        assertThat(productsDto).extracting("id").containsSequence(1L, 2L, 3L, 4L)
    }

}
