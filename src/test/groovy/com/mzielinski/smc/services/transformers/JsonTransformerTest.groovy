package com.mzielinski.smc.services.transformers

import spock.lang.Specification

class JsonTransformerTest extends Specification {

    class SampleObject {
        int property1
        String property2
    }

    JsonTransformer objectUnderTest = new JsonTransformer();

    def "should convert any object to JSON"() {
        given:
            SampleObject object = new SampleObject(property1: 12, property2: "some data")

        when:
            String json = objectUnderTest.render(object)

        then:
            json == "{\"property1\":12,\"property2\":\"some data\"}"
    }

}
