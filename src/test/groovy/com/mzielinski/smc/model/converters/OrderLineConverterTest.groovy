package com.mzielinski.smc.model.converters

import com.mzielinski.smc.model.dtos.OrderLineDto
import com.mzielinski.smc.model.entities.OrderLine
import com.mzielinski.smc.model.entities.Product
import com.mzielinski.smc.repositories.ProductRepository
import spock.lang.Specification

class OrderLineConverterTest extends Specification {

    ProductRepository productRepositoryMock = Mock(ProductRepository)

    OrderLineConverter objectUnderTest = new OrderLineConverter(productRepositoryMock);

    def "should convert OrderLine to OrderLineDto"() {
        given:
        Product product = Product.builder().build()
        OrderLine orderLine = OrderLine.builder().product(product.id).quantity(10).subtotal(new BigDecimal("12.32")).build()

        when:
        OrderLineDto orderLineDto = objectUnderTest.convert(orderLine)

        then:
        orderLineDto.product == product.id
        orderLineDto.quantity == orderLine.quantity
        orderLineDto.subtotal == orderLine.subtotal
    }

    def "should return null when OrderLine entity is null"() {
        expect:
        objectUnderTest.convert(null) == null
    }

    def "should convert OrderLineDto to OrderLine"() {
        given:
        Product product = Product.builder().build()
        OrderLineDto orderLineDto = OrderLineDto.builder().product(product.id).quantity(10).subtotal(new BigDecimal("12.32")).build()
        productRepositoryMock.findOne(product.id) >> product

        when:
        OrderLine orderLine = objectUnderTest.reverse().convert(orderLineDto)

        then:
        orderLine.product == product.id
        orderLine.quantity == orderLine.quantity
        orderLine.subtotal == orderLine.subtotal
    }

    def "should return null when OrderLine DTO is null"() {
        expect:
        objectUnderTest.reverse().convert(null) == null
    }

}
