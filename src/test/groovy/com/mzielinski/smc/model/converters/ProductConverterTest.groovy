package com.mzielinski.smc.model.converters

import com.mzielinski.smc.model.dtos.ProductDto
import com.mzielinski.smc.model.entities.Product
import com.mzielinski.smc.model.entities.WarehouseItem
import com.mzielinski.smc.repositories.ProductRepository
import com.mzielinski.smc.repositories.WarehouseRepository
import spock.lang.Specification

class ProductConverterTest extends Specification {

    WarehouseRepository warehouseRepositoryMock = Mock(WarehouseRepository)
    ProductRepository productRepositoryMock = Mock(ProductRepository)

    ProductConverter objectUnderTest = new ProductConverter(warehouseRepositoryMock, productRepositoryMock);

    def "should convert Product to ProductDto"() {
        given:
        def product = Product.builder().name("name").price(new BigDecimal("12.32")).build()
        def warehouseItem = WarehouseItem.builder().quantity(213).build()
        warehouseRepositoryMock.findByProduct(product.id) >> warehouseItem

        when:
        ProductDto productDto = objectUnderTest.convert(product)

        then:
        productDto.id == product.id
        productDto.name == product.name
        productDto.quantity == warehouseItem.quantity
        productDto.price == product.price
    }

    def "should return null when Product entity is null"() {
        expect:
        objectUnderTest.convert(null) == null
    }

    def "should convert ProductDto to Product"() {
        given:
        ProductDto productDto = ProductDto.builder().id(10).name("name").quantity(20).price(new BigDecimal("12.32")).build()
        productRepositoryMock.findOne(productDto.id) >> Product.builder().id(productDto.id).build()

        when:
        Product product = objectUnderTest.reverse().convert(productDto);

        then:
        product.id == productDto.id
        product.name == productDto.name
        product.price == productDto.price
    }

    def "should return null when Product DTO is null"() {
        expect:
        objectUnderTest.reverse().convert(null) == null
    }

}
