package com.mzielinski.smc.model.entities.validation

import com.mzielinski.smc.model.entities.Product
import spock.lang.Specification

import javax.validation.ConstraintViolation
import javax.validation.Validation
import javax.validation.Validator
import javax.validation.ValidatorFactory

import static org.assertj.core.api.Assertions.assertThat

class ProductValidationTest extends Specification {

    Validator validator

    def setup() {
        ValidatorFactory vf = Validation.buildDefaultValidatorFactory()
        validator = vf.getValidator()
    }

    def "should generate constraint validation when name is invalid"() {
        given:
        def product = Product.builder()
                .name("")
                .price(BigDecimal.ZERO)
                .build()

        when:
        Set<ConstraintViolation<Product>> violations  = validator.validate(product)

        then:
        assertThat(violations).extracting("messageTemplate")
                .containsExactly("{javax.validation.constraints.Size.message}")
    }

    def "should generate constraint validation when description is invalid"() {
        given:
        def product = Product.builder()
                .name("valid name")
                .description("*" * 256)
                .price(BigDecimal.ZERO)
                .build()

        when:
        Set<ConstraintViolation<Product>> violations  = validator.validate(product)

        then:
        assertThat(violations).extracting("messageTemplate")
                .containsExactly("{javax.validation.constraints.Size.message}")
    }

    def "should generate constraint validation when price is invalid"() {
        given:
        def product = Product.builder()
                .name("valid name")
                .price(new BigDecimal("-1.00"))
                .build()

        when:
        Set<ConstraintViolation<Product>> violations  = validator.validate(product)

        then:
        assertThat(violations).extracting("messageTemplate")
                .containsExactly("{javax.validation.constraints.DecimalMin.message}")
    }

}
