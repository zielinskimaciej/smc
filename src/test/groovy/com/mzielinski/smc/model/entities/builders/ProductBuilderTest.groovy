package com.mzielinski.smc.model.entities.builders

import com.mzielinski.smc.enums.ProductType
import com.mzielinski.smc.model.entities.Product
import spock.lang.Specification

import java.time.LocalDateTime
import java.time.Month

class ProductBuilderTest extends Specification {

    private LocalDateTime defaultCreateDate = LocalDateTime.of(2014, Month.DECEMBER, 12, 10, 10)

    def "should build product with all passed parameters"() {
        when:
        //@formatter:off
        def product = Product.builder()
                .id(1)
                .createDate(defaultCreateDate)
                .name("name")
                .description("desc")
                .price(BigDecimal.ONE)
                .type(ProductType.TYPE1)
                .build()
        //@formatter:on

        then:
        product.id == 1
        product.createDate == defaultCreateDate
        product.name == "name"
        product.description == "desc"
        product.price == BigDecimal.ONE
        product.type == ProductType.TYPE1
    }

    def "should copy product to new object with same values"() {
        given:
        //@formatter:off
        def product = Product.builder()
                .id(1)
                .createDate(defaultCreateDate)
                .name("name")
                .description("desc")
                .price(BigDecimal.ONE)
                .type(ProductType.TYPE1)
                .build()
        //@formatter:on

        when:
        Product copy = Product.copy(product).build()

        then:
        assert !copy.is(product)
        copy.id == product.id
        copy.createDate == product.createDate
        copy.name == product.name
        copy.description == product.description
        copy.price == product.price
        copy.type == product.type
    }

}
