package com.mzielinski.smc.model.entities.validation

import com.mzielinski.smc.model.entities.Order
import spock.lang.Specification

import javax.validation.ConstraintViolation
import javax.validation.Validation
import javax.validation.Validator
import javax.validation.ValidatorFactory

import static org.assertj.core.api.Assertions.assertThat

class OrderValidationTest extends Specification {

    Validator validator

    def setup() {
        ValidatorFactory vf = Validation.buildDefaultValidatorFactory()
        validator = vf.getValidator()
    }

    def "should generate constraint validation when total price is invalid"() {
        given:
        def order = Order.builder()
                .lines([])
                .totalPrice(new BigDecimal("-1.00"))
                .build()

        when:
        Set<ConstraintViolation<Order>> violations  = validator.validate(order)

        then:
        assertThat(violations).extracting("messageTemplate")
                .containsExactly("{javax.validation.constraints.DecimalMin.message}")
    }
    
}
