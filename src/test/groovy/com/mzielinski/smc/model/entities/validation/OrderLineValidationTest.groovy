package com.mzielinski.smc.model.entities.validation

import com.mzielinski.smc.model.entities.OrderLine
import com.mzielinski.smc.model.entities.Product
import spock.lang.Specification

import javax.validation.ConstraintViolation
import javax.validation.Validation
import javax.validation.Validator
import javax.validation.ValidatorFactory

import static org.assertj.core.api.Assertions.assertThat

class OrderLineValidationTest extends Specification {

    Validator validator

    def setup() {
        ValidatorFactory vf = Validation.buildDefaultValidatorFactory()
        validator = vf.getValidator()
    }

    def "should generate constraint validation when product is invalid"() {
        given:
        def orderLine = OrderLine.builder()
                .subtotal(BigDecimal.TEN)
                .quantity(10)
                .build()

        when:
        Set<ConstraintViolation<OrderLine>> violations  = validator.validate(orderLine)

        then:
        assertThat(violations).extracting("messageTemplate")
                .containsExactly("{javax.validation.constraints.Min.message}")
    }

    def "should generate constraint validation when quantity is invalid"() {
        given:
        def orderLine = OrderLine.builder()
                .subtotal(BigDecimal.TEN)
                .product(Product.builder().build().id)
                .build()

        when:
        Set<ConstraintViolation<OrderLine>> violations  = validator.validate(orderLine)

        then:
        assertThat(violations).extracting("messageTemplate")
                .containsExactly("{javax.validation.constraints.Min.message}")
    }

    def "should generate constraint validation when subtotal is invalid"() {
        given:
        def orderLine = OrderLine.builder()
                .subtotal(new BigDecimal("-10.00"))
                .product(Product.builder().build().id)
                .quantity(10)
                .build()

        when:
        Set<ConstraintViolation<OrderLine>> violations  = validator.validate(orderLine)

        then:
        assertThat(violations).extracting("messageTemplate")
                .containsExactly("{javax.validation.constraints.DecimalMin.message}")
    }

}
