package com.mzielinski.smc.model.entities.builders

import com.google.common.collect.ImmutableList
import com.mzielinski.smc.model.entities.Order
import com.mzielinski.smc.model.entities.OrderLine
import com.mzielinski.smc.model.entities.Product
import spock.lang.Specification

class OrderBuilderTest extends Specification {

    def "should build order with all passed parameters"() {
        given:
        def product = Product.builder().build()
        def lines = [OrderLine.builder().product(product.id).build()]

        when:
        //@formatter:off
        def order = Order.builder()
            .totalPrice(BigDecimal.TEN)
            .lines(lines)
            .build()
        //@formatter:on

        then:
        order.id > 0
        order.totalPrice == BigDecimal.TEN
        order.lines == lines
        order.lines instanceof ImmutableList
    }

    def "should build order with immutable empty list of lines when user did not set list by himself"() {
        when:
        def order = Order.builder().build()

        then:
        order.lines.size() == 0
        order.lines instanceof ImmutableList
    }

}
