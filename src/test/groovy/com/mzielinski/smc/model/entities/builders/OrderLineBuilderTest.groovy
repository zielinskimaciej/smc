package com.mzielinski.smc.model.entities.builders

import com.mzielinski.smc.model.entities.OrderLine
import com.mzielinski.smc.model.entities.Product
import spock.lang.Specification

class OrderLineBuilderTest extends Specification {

    def "should build order with all passed parameters"() {
        given:
        def product = Product.builder().build()

        when:
        def orderLine = OrderLine.builder()
                .product(product.id)
                .subtotal(BigDecimal.TEN)
                .quantity(10)
                .build()

        then:
        orderLine.id > 0
        orderLine.product == product.id
        orderLine.quantity == 10
        orderLine.subtotal == BigDecimal.TEN
    }

}
