package com.mzielinski.smc.model.entities.validation

import com.mzielinski.smc.exceptions.NotEnoughItemsException
import com.mzielinski.smc.model.entities.Product
import com.mzielinski.smc.model.entities.WarehouseItem
import spock.lang.FailsWith
import spock.lang.Specification

import javax.validation.ConstraintViolation
import javax.validation.Validation
import javax.validation.Validator
import javax.validation.ValidatorFactory

import static org.assertj.core.api.Assertions.assertThat

class WarehouseItemValidationTest extends Specification {

    Validator validator

    def setup() {
        ValidatorFactory vf = Validation.buildDefaultValidatorFactory()
        validator = vf.getValidator()
    }

    def "should generate constraint validation when product is invalid"() {
        given:
        def item = WarehouseItem.builder()
                .quantity(10)
                .build()

        when:
        Set<ConstraintViolation<WarehouseItem>> violations  = validator.validate(item)

        then:
        assertThat(violations).extracting("messageTemplate")
                .containsExactly("{javax.validation.constraints.Min.message}")
    }

    @FailsWith(value = NotEnoughItemsException)
    def "should generate constraint validation when quantity is invalid"() {
        expect:
        WarehouseItem.builder()
                .product(Product.builder().build().id)
                .quantity(-1)
                .build()
    }

}
