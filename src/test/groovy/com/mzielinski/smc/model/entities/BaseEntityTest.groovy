package com.mzielinski.smc.model.entities

import spock.lang.Specification

import static com.mzielinski.smc.enums.ProductType.TYPE1
import static com.mzielinski.smc.enums.ProductType.TYPE2

class BaseEntityTest extends Specification {

    def "should compare two products only by id"() {
        given:
        Product firstProduct = Product.builder()
                .id(1)
                .name("name1")
                .description("desc1")
                .price(BigDecimal.TEN)
                .type(TYPE1)
                .build()
        Product secondProduct = Product.builder()
                .id(1)
                .name("name2")
                .description("desc2")
                .price(BigDecimal.ONE)
                .type(TYPE2)
                .build()

        expect:
        firstProduct == secondProduct;
    }

}
