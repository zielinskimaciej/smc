import org.openqa.selenium.chrome.ChromeDriver

waiting {
    timeout = 5
}

environments {

    chrome {
        driver = { new ChromeDriver() }
    }

}

baseUrl = "http://127.0.0.1:9393/test"