package com.mzielinski.smc.datasource;

import com.google.common.collect.Sets;
import com.mzielinski.smc.enums.ProductType;
import com.mzielinski.smc.exceptions.SmcAdministrationException;
import com.mzielinski.smc.model.entities.Order;
import com.mzielinski.smc.model.entities.Product;
import com.mzielinski.smc.model.entities.WarehouseItem;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.management.JMException;
import javax.management.MBeanServer;
import javax.management.ObjectName;
import java.math.BigDecimal;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.mzielinski.smc.enums.ProductType.TYPE1;
import static com.mzielinski.smc.enums.ProductType.TYPE2;

@Slf4j
@Singleton
public class DataSource implements DataSourceMBean {

    public static final String DATA_SOURCE_MBEAN_NAME = "MemoryDataSource:type=MemoryDataSource";

    private static final AtomicLong SEQUENCE_ID = new AtomicLong(1L);
    private static final int DEFAULT_ITEM1_QUANTITY = 20;
    private static final int DEFAULT_ITEM2_QUANTITY = 10;

    private final Set<Product> products = Sets.newConcurrentHashSet();
    private final Set<Order> orders = Sets.newConcurrentHashSet();
    private final Set<WarehouseItem> warehouse = Sets.newConcurrentHashSet();

    @Override
    public synchronized Product selectProduct(long product) {
        checkArgument(product > 0, "product identifier has to be greater then zero");

        log.info("Selecting product with id [{}] from data source", product);
        //@formatter:off
        final Optional<Product> result = products.stream()
                .filter(p -> p.getId() == product)
                .findFirst();
        return result.isPresent() ? result.get() : null;
        //@formatter:on
    }

    @Override
    public synchronized Iterable<Product> selectProducts() {
        log.info("Selecting all products from data source");
        return products;
    }

    @Override
    public synchronized Product insertProduct(Product product) {
        checkNotNull(product, "product cannot be null");

        log.info("Inserting product {} to data source", product);
        products.add(product);
        return product;
    }

    @Override
    public synchronized Product updateProduct(Product product) {
        checkNotNull(product, "product cannot be null");

        log.info("Updating product {} in data source", product);
        products.remove(product);
        products.add(product);
        return product;
    }

    @Override
    public synchronized WarehouseItem insertWarehouse(WarehouseItem item) {
        checkNotNull(item, "warehouse cannot be null");

        log.info("Inserting warehouse item {} to data source", item);
        warehouse.add(item);
        return item;
    }

    @Override
    public synchronized WarehouseItem updateWarehouse(WarehouseItem item) {
        checkNotNull(item, "warehouseItem cannot be null");

        log.info("Updating warehouse item {} in data source", item);
        warehouse.remove(item);
        warehouse.add(item);
        return item;
    }

    @Override
    public synchronized Iterable<WarehouseItem> selectWarehouse() {
        log.info("Selecting all orders from data source");
        return warehouse;
    }

    @Override
    public synchronized WarehouseItem selectWarehouseByProduct(long product) {
        checkArgument(product > 0, "product identifier has to be greater then zero");

        log.info("Selecting warehouse item with product id [{}] from data source", product);
        //@formatter:off
        final Optional<WarehouseItem> result = warehouse.stream()
                .filter(item -> item.getProduct() == product)
                .findFirst();
        return result.isPresent() ? result.get() : null;
        //@formatter:on
    }


    @Override
    public synchronized Iterable<Order> selectOrders() {
        log.info("Selecting all orders from data source");
        return orders;
    }

    @Override
    public synchronized Order insertOrder(Order order) {
        checkNotNull(order, "order cannot be null");

        log.info("Adding order {} with lines to data source", order);
        orders.add(order);
        return order;
    }

    @Override
    public synchronized void clear() {
        log.info("Clearing all data in data source");

        products.clear();
        orders.clear();
        warehouse.clear();
    }

    @Inject
    public void register(MBeanServer server) {
        try {
            server.registerMBean(this, new ObjectName(DATA_SOURCE_MBEAN_NAME));
        } catch (JMException e) {
            throw new SmcAdministrationException(e);
        }
    }

    public static long getNextSequenceId() {
        return SEQUENCE_ID.getAndIncrement();
    }

    @PostConstruct
    public void createInitData() {
        insertProduct("item 1", "desc of item 1", TYPE1, new BigDecimal("100.0"), DEFAULT_ITEM1_QUANTITY);
        insertProduct("item 2", "desc of item 2", TYPE2, new BigDecimal("200.0"), DEFAULT_ITEM2_QUANTITY);
    }

    //@formatter:off
    private void insertProduct(String name, String desc, ProductType type, BigDecimal price, int quantity) {
        Product product = insertProduct(Product.builder()
                .name(name)
                .description(desc)
                .type(type)
                .price(price)
                .build());
        insertWarehouse(WarehouseItem.builder()
                .product(product.getId())
                .quantity(quantity)
                .build());
    }
    //@formatter:on

}
