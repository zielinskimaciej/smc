package com.mzielinski.smc.datasource;

import com.mzielinski.smc.model.entities.Order;
import com.mzielinski.smc.model.entities.Product;
import com.mzielinski.smc.model.entities.WarehouseItem;

public interface DataSourceMBean {

    Iterable<Product> selectProducts();

    Product selectProduct(long product);

    Product insertProduct(Product product);

    Product updateProduct(Product product);

    Iterable<Order> selectOrders();

    Order insertOrder(Order order);

    Iterable<WarehouseItem> selectWarehouse();

    WarehouseItem selectWarehouseByProduct(long product);

    WarehouseItem insertWarehouse(WarehouseItem warehouseItem);

    WarehouseItem updateWarehouse(WarehouseItem warehouseItem);

    void clear();

}
