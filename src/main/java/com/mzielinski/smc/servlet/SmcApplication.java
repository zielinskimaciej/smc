package com.mzielinski.smc.servlet;

import com.mzielinski.smc.servlet.enpoints.ConstantsEndpoint;
import com.mzielinski.smc.servlet.enpoints.ExceptionEndpoint;
import com.mzielinski.smc.servlet.enpoints.OrderEndpoint;
import com.mzielinski.smc.servlet.enpoints.ProductEndpoint;
import spark.servlet.SparkApplication;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class SmcApplication implements SparkApplication {

    private final ProductEndpoint productEndpoint;
    private final OrderEndpoint orderEndpoint;
    private final ExceptionEndpoint exceptionEndpoint;
    private final ConstantsEndpoint constantsEndpoint;

    @Inject
    public SmcApplication(ProductEndpoint product, OrderEndpoint order, ExceptionEndpoint exception, ConstantsEndpoint constants) {
        this.productEndpoint = product;
        this.orderEndpoint = order;
        this.exceptionEndpoint = exception;
        this.constantsEndpoint = constants;
    }

    @Override
    public void init() {
        productEndpoint.init();
        orderEndpoint.init();
        exceptionEndpoint.init();
        constantsEndpoint.init();
    }

}
