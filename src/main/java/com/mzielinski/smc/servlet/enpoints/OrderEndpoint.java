package com.mzielinski.smc.servlet.enpoints;

import com.google.gson.Gson;
import com.mzielinski.smc.model.dtos.OrderDto;
import com.mzielinski.smc.model.dtos.ResponseWrapper;
import com.mzielinski.smc.model.entities.Order;
import com.mzielinski.smc.services.MessagesService;
import com.mzielinski.smc.services.OrderService;
import com.mzielinski.smc.services.transformers.JsonTransformer;
import lombok.extern.slf4j.Slf4j;
import spark.Request;
import spark.servlet.SparkApplication;

import javax.inject.Inject;
import javax.inject.Singleton;

import static com.mzielinski.smc.enums.ContentType.JSON;
import static spark.Spark.post;

@Slf4j
@Singleton
public class OrderEndpoint implements SparkApplication {

    private final Gson gson = new Gson();

    private final OrderService orderService;
    private final MessagesService messagesService;

    @Inject
    public OrderEndpoint(OrderService orderService, MessagesService messagesService) {
        this.orderService = orderService;
        this.messagesService = messagesService;
    }

    @Override
    public void init() {
        post("/order", (req, res) -> {
            final String json = getJson(req);
            final OrderDto orderDto = getOrderDto(json);
            final Order order = orderService.processOrder(orderDto);
            res.type(JSON.getType());
            return ResponseWrapper.builder()
                    .message(messagesService.getMessage("message.success", order.getId()))
                    .build();
        }, new JsonTransformer());
    }

    private String getJson(Request req) {
        final String json = req.body();
        log.info("Received JSON order: {}", req.body());
        return json;
    }

    private OrderDto getOrderDto(String json) {
        final OrderDto orderDto = gson.fromJson(json, OrderDto.class);
        log.info("Converted object: {}", orderDto);
        return orderDto;
    }

}
