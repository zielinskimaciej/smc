package com.mzielinski.smc.servlet.enpoints;

import com.mzielinski.smc.model.dtos.ResponseWrapper;
import com.mzielinski.smc.services.ProductService;
import com.mzielinski.smc.services.transformers.JsonTransformer;
import spark.servlet.SparkApplication;

import javax.inject.Inject;
import javax.inject.Singleton;

import static com.mzielinski.smc.enums.ContentType.JSON;
import static spark.Spark.get;

@Singleton
public class ProductEndpoint implements SparkApplication {

    private final ProductService productService;

    @Inject
    public ProductEndpoint(ProductService productService) {
        this.productService = productService;
    }

    @Override
    public void init() {
        get("/products", (req, res) -> {
            res.type(JSON.getType());
            return ResponseWrapper.builder()
                    .object(productService.findAllProducts())
                    .build();
        }, new JsonTransformer());
    }

}
