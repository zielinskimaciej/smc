package com.mzielinski.smc.servlet.enpoints;

import com.google.inject.Singleton;
import com.mzielinski.smc.services.MessagesService;
import spark.servlet.SparkApplication;

import javax.inject.Inject;

import static spark.Spark.get;

@Singleton
public class ConstantsEndpoint implements SparkApplication {

    private final MessagesService messagesService;

    @Inject
    public ConstantsEndpoint(MessagesService messagesService) {
        this.messagesService = messagesService;
    }

    @Override
    public void init() {
        get("/constant/:code", (req, res) -> {
            final String code = req.params(":code");
            return messagesService.getMessage(code);
        });
    }

}
