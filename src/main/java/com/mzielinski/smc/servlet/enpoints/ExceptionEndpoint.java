package com.mzielinski.smc.servlet.enpoints;

import static com.mzielinski.smc.enums.ContentType.JSON;
import static com.mzielinski.smc.model.dtos.ResponseWrapper.Status;
import static spark.Spark.exception;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.validation.ConstraintViolationException;

import lombok.extern.slf4j.Slf4j;
import spark.Response;
import spark.servlet.SparkApplication;

import com.google.gson.Gson;
import com.mzielinski.smc.exceptions.NotEnoughItemsException;
import com.mzielinski.smc.model.dtos.ResponseWrapper;
import com.mzielinski.smc.services.MessagesService;

@Slf4j
@Singleton
public class ExceptionEndpoint implements SparkApplication {

    private final Gson gson = new Gson();

    private final MessagesService messagesService;

    @Inject
    public ExceptionEndpoint(MessagesService messagesService) {
        this.messagesService = messagesService;
    }

    @Override
    public void init() {

        /**
         * Process {@link NotEnoughItemsException} exceptions
         */
        exception(NotEnoughItemsException.class, (e, request, response) ->
                handleException(e, response, Status.WARNING, "message.NotEnoughItemsException"));

        /**
         * Process {@link ConstraintViolationException} exceptions
         */
        exception(ConstraintViolationException.class, (e, request, response) ->
                handleException(e, response, Status.ERROR, "message.Exception"));

        /**
         * Process all unhandled exceptions
         */
        exception(Exception.class, (e, request, response) ->
                handleException(e, response, Status.ERROR, "message.Exception"));

    }

    private void handleException(Exception e, Response response, Status status, String message) {
        log.error(e.getMessage(), e);

        final ResponseWrapper responseWrapper = ResponseWrapper.builder()
                .status(status)
                .message(messagesService.getMessage(message))
                .build();
        log.info("Response with exception {} ", responseWrapper);
        response.type(JSON.getType());
        response.body(gson.toJson(responseWrapper));
    }

}
