package com.mzielinski.smc.servlet;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.mycila.guice.ext.service.ServiceModule;
import spark.servlet.SparkApplication;
import spark.servlet.SparkFilter;

import javax.servlet.FilterConfig;
import javax.servlet.ServletException;

public class SmcSparkFilter extends SparkFilter {

    @Override
    protected SparkApplication getApplication(FilterConfig filterConfig) throws ServletException {
        try {
            final Injector injector = Guice.createInjector(new ServiceModule());
            return injector.getInstance(SmcApplication.class);
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

}
