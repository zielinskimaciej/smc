package com.mzielinski.smc.enums;

public enum ContentType {

    JSON("application/json"),
    TEXT("text/plain"),
    XML("application/xml"),
    HTML("text/html"),
    BINARY("application/octet-stream");

    private final String type;

    ContentType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

}
