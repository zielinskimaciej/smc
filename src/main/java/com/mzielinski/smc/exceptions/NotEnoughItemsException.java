package com.mzielinski.smc.exceptions;

import lombok.Getter;

@Getter
public class NotEnoughItemsException extends RuntimeException {

    public NotEnoughItemsException(long productId, int quantity) {
        super(String.format("product [%d], quantity [%d]", productId, quantity));
    }

}
