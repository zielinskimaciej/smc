package com.mzielinski.smc.exceptions;

public class UnknownProductException extends RuntimeException {

    public UnknownProductException(long product) {
        super(String.format("Unknown product with id [%d]", product));
    }

}
