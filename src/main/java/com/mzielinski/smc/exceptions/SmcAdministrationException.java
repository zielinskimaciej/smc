package com.mzielinski.smc.exceptions;

import javax.management.JMException;

public class SmcAdministrationException extends RuntimeException {

    public SmcAdministrationException(JMException e) {
        super(e);
    }

}
