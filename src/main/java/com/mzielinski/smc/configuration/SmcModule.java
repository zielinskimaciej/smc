package com.mzielinski.smc.configuration;

import com.google.inject.AbstractModule;
import com.mzielinski.smc.datasource.DataSource;
import com.mzielinski.smc.datasource.DataSourceMBean;

import javax.management.MBeanServer;
import java.lang.management.ManagementFactory;

public class SmcModule extends AbstractModule {

    @Override
    protected void configure() {
        binder().disableCircularProxies();

        bind(MBeanServer.class).toInstance(ManagementFactory.getPlatformMBeanServer());
        bind(DataSourceMBean.class).to(DataSource.class).asEagerSingleton();
    }
}
