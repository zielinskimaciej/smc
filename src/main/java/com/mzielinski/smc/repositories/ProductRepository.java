package com.mzielinski.smc.repositories;

import com.mzielinski.smc.exceptions.UnknownProductException;
import com.mzielinski.smc.model.entities.Product;
import org.apache.bval.guice.Validate;

import javax.annotation.Nonnull;
import javax.inject.Singleton;
import javax.validation.Valid;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

@Singleton
public class ProductRepository extends BaseRepository {
    
    public Iterable<Product> findAll() {
        return getDataSource().selectProducts();
    }

    public Product findOne(long productId) {
        checkArgument(productId > 0, "product identifier has to be greater then zero");

        final Product product = getDataSource().selectProduct(productId);
        if (product != null) {
            return product;
        }
        throw new UnknownProductException(productId);
    }

    @Validate
    public Product save(@Valid @Nonnull Product product) {
        checkNotNull(product, "product cannot be null");

        return getDataSource().updateProduct(product);
    }

}
