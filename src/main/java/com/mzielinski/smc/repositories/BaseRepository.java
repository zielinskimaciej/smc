package com.mzielinski.smc.repositories;

import com.mzielinski.smc.datasource.DataSourceMBean;

import javax.annotation.Resource;

public abstract class BaseRepository {

    private DataSourceMBean dataSource;

    @Resource
    public void setDataSource(DataSourceMBean dataSource) {
        this.dataSource = dataSource;
    }

    protected DataSourceMBean getDataSource() {
        return dataSource;
    }

}
