package com.mzielinski.smc.repositories;

import com.mzielinski.smc.model.entities.Order;
import org.apache.bval.guice.Validate;

import javax.annotation.Nonnull;
import javax.inject.Singleton;
import javax.validation.Valid;

import static com.google.common.base.Preconditions.checkNotNull;

@Singleton
public class OrderRepository extends BaseRepository {

    @Validate
    public Order save(@Valid @Nonnull Order order) {
        checkNotNull(order, "order cannot be null");

        return getDataSource().insertOrder(order);
    }

}
