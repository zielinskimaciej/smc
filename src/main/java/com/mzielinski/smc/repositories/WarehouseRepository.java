package com.mzielinski.smc.repositories;

import com.mzielinski.smc.model.entities.WarehouseItem;
import org.apache.bval.guice.Validate;

import javax.annotation.Nonnull;
import javax.inject.Singleton;
import javax.validation.Valid;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

@Singleton
public class WarehouseRepository extends BaseRepository {

    public WarehouseItem findByProduct(long product) {
        checkArgument(product > 0, "product identifier has to be greater then zero");

       return getDataSource().selectWarehouseByProduct(product);
    }

    @Validate
    public WarehouseItem save(@Valid @Nonnull WarehouseItem item) {
        checkNotNull(item, "warehouse item cannot be null");

        return getDataSource().updateWarehouse(item);
    }

}
