package com.mzielinski.smc.services;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Strings.isNullOrEmpty;

import java.text.MessageFormat;
import java.util.ResourceBundle;

import javax.annotation.Nonnull;
import javax.inject.Singleton;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Singleton
public class MessagesService {

    private final ResourceBundle resourceBundle;

    public MessagesService() {
        this("messages");
    }

    public MessagesService(String fileName) {
        resourceBundle = ResourceBundle.getBundle(fileName);
    }

    public String getMessage(@Nonnull String code, Object... params) {
        checkArgument(!isNullOrEmpty(code), "code cannot be blank");

        log.info("Try to find code [{}] in message.properties", code);
        final String message = resourceBundle.getString(code);
        return MessageFormat.format(message, params);
    }

}
