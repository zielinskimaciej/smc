package com.mzielinski.smc.services;

import com.mzielinski.smc.model.converters.OrderLineConverter;
import com.mzielinski.smc.model.dtos.OrderDto;
import com.mzielinski.smc.model.dtos.OrderLineDto;
import com.mzielinski.smc.model.entities.Order;
import com.mzielinski.smc.model.entities.OrderLine;
import com.mzielinski.smc.repositories.OrderRepository;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.Nonnull;
import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.stream.Collectors.toList;

@Slf4j
@Singleton
public class OrderService {

    private final WarehouseService warehouseService;
    private final OrderRepository orderRepository;
    private final OrderLineConverter orderLineConverter;

    @Inject
    public OrderService(WarehouseService warehouseService, OrderRepository orderRepository, OrderLineConverter orderLineConverter) {
        this.warehouseService = warehouseService;
        this.orderRepository = orderRepository;
        this.orderLineConverter = orderLineConverter;
    }

    /**
     * Method is responsible for ordering process
     *
     * @param orderDto
     *         order retrieved from UI
     */
    public synchronized Order processOrder(@Nonnull OrderDto orderDto) {
        checkNotNull(orderDto, "order cannot be null");
        checkNotNull(orderDto.getLines(), "order lines cannot be null");

        final Order order = buildOrder(orderDto);
        decrementProductQuantities(order.getLines());
        return orderRepository.save(order);
    }

    private Order buildOrder(OrderDto orderDto) {
        final List<OrderLine> orderLines = buildOrderLines(orderDto.getLines());
        return Order.builder()
                .totalPrice(orderDto.getTotalPrice())
                .lines(orderLines)
                .build();
    }

    private List<OrderLine> buildOrderLines(List<OrderLineDto> orderLinesDto) {
        return orderLinesDto.stream()
                .map(line -> orderLineConverter.reverse().convert(line))
                .collect(toList());
    }

    private void decrementProductQuantities(List<OrderLine> orderLinesDto) {
        orderLinesDto.stream()
                .forEach(line -> warehouseService.decrementProductQuantity(line.getProduct(), line.getQuantity()));
    }

}
