package com.mzielinski.smc.services;

import com.mzielinski.smc.model.converters.ProductConverter;
import com.mzielinski.smc.model.dtos.ProductDto;
import com.mzielinski.smc.model.entities.Product;
import com.mzielinski.smc.model.entities.comparators.CreateDateComparator;
import com.mzielinski.smc.repositories.ProductRepository;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static java.util.stream.StreamSupport.stream;

@Slf4j
@Singleton
public class ProductService {

    private ProductRepository productRepository;
    private ProductConverter productConverter;

    @Inject
    public ProductService(ProductRepository productRepository, ProductConverter productConverter) {
        this.productRepository = productRepository;
        this.productConverter = productConverter;
    }

    /**
     * @return all products fetched from repository layer and converts to @{line ProductDto} product dto object list.
     */
    public List<ProductDto> findAllProducts() {
        final Iterable<Product> products = productRepository.findAll();
        return stream(products.spliterator(), false)
                .sorted(new CreateDateComparator()::compare)
                .map(productConverter::convert)
                .collect(toList());
    }

}
