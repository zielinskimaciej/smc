package com.mzielinski.smc.services;

import com.mzielinski.smc.model.entities.WarehouseItem;
import com.mzielinski.smc.repositories.WarehouseRepository;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Inject;
import javax.inject.Singleton;

import static com.google.common.base.Preconditions.checkArgument;

@Slf4j
@Singleton
public class WarehouseService {

    private final WarehouseRepository warehouseRepository;

    @Inject
    public WarehouseService(WarehouseRepository warehouseRepository) {
        this.warehouseRepository = warehouseRepository;
    }

    /**
     * @param product
     *         product identifier
     * @param quantity
     *         number of items which were bought
     */
    public synchronized WarehouseItem decrementProductQuantity(long product, int quantity) {
        checkArgument(product > 0, String.format("product identifier has to be positive [%d]", product));
        checkArgument(quantity > 0, String.format("quantity has to be positive. Value [%d]", quantity));

        final WarehouseItem warehouseItem = warehouseRepository.findByProduct(product);
        final WarehouseItem updatedWarehouseItem = WarehouseItem.copy(warehouseItem)
                .quantity(warehouseItem.getQuantity() - quantity)
                .build();
        return warehouseRepository.save(updatedWarehouseItem);
    }

}
