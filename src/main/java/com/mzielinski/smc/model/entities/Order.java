package com.mzielinski.smc.model.entities;

import com.google.common.collect.ImmutableList;
import lombok.Getter;
import lombok.ToString;
import lombok.experimental.Builder;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import static java.util.Collections.emptyList;

@Getter
@ToString(callSuper = true)
public class Order extends BaseEntity {

    @NotNull
    private final List<OrderLine> lines;

    @DecimalMin("0.00")
    private final BigDecimal totalPrice;

    @Builder
    private Order(long id, LocalDateTime createDate, List<OrderLine> lines, BigDecimal totalPrice) {
        super(id, createDate);
        this.totalPrice = totalPrice;
        this.lines = new ImmutableList.Builder<OrderLine>()
                .addAll(lines != null ? lines : emptyList())
                .build();
    }

}
