package com.mzielinski.smc.model.entities;

import java.time.LocalDateTime;

import javax.validation.constraints.Min;

import lombok.Getter;
import lombok.ToString;
import lombok.experimental.Builder;

import com.mzielinski.smc.exceptions.NotEnoughItemsException;

@Getter
@ToString(callSuper = true)
public class WarehouseItem extends BaseEntity {

    @Min(1)
    private final long product;

    @Min(0)
    private final int quantity;

    @Builder
    private WarehouseItem(long id, LocalDateTime createDate, long product, int quantity) {
        super(id, createDate);
        verifyQuantity(product, quantity);

        this.product = product;
        this.quantity = quantity;
    }

    private void verifyQuantity(long product, int quantity) {
        if (quantity < 0) {
            throw new NotEnoughItemsException(product, quantity);
        }
    }

    public static WarehouseItemBuilder copy(WarehouseItem item) {
        // @formatter:off
        return new WarehouseItemBuilder()
                .id(item.getId())
                .createDate(item.getCreateDate())
                .product(item.getProduct())
                .quantity(item.getQuantity());
        // @formatter:on
    }

}
