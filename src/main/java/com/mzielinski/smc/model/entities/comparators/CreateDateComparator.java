package com.mzielinski.smc.model.entities.comparators;

import com.mzielinski.smc.model.entities.BaseEntity;

import javax.annotation.Nonnull;
import java.util.Comparator;

import static com.google.common.base.Preconditions.checkNotNull;

public class CreateDateComparator implements Comparator<BaseEntity> {

    @Override
    public int compare(@Nonnull BaseEntity e1, @Nonnull BaseEntity e2) {
        checkNotNull(e1, "first entity cannot be null");
        checkNotNull(e1.getCreateDate(), "first entity create date cannot be null");
        checkNotNull(e2, "second entity cannot be null");
        checkNotNull(e2.getCreateDate(), "second entity create date cannot be null");

        return e1.getCreateDate().compareTo(e2.getCreateDate());
    }

}
