package com.mzielinski.smc.model.entities;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.ToString;
import lombok.experimental.Builder;

import com.mzielinski.smc.enums.ProductType;

@Getter
@ToString(callSuper = true)
public class Product extends BaseEntity {

    @Size(min = 1, max = 16)
    private final String name;

    @Size(min = 0, max = 255)
    private final String description;

    @DecimalMin("0.00")
    private final BigDecimal price;

    private final ProductType type;

    @Builder
    private Product(long id, LocalDateTime createDate, String name, String description, BigDecimal price, ProductType type) {
        super(id, createDate);
        this.name = name;
        this.description = description;
        this.price = price;
        this.type = type;
    }

    public static ProductBuilder copy(Product product) {
        // @formatter:off
        return new ProductBuilder()
                .id(product.getId())
                .createDate(product.getCreateDate())
                .type(product.getType())
                .name(product.getName())
                .description(product.getDescription())
                .price(product.getPrice());
        // @formatter:on
    }

}
