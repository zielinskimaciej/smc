package com.mzielinski.smc.model.entities;

import lombok.Getter;
import lombok.ToString;
import lombok.experimental.Builder;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@ToString(callSuper = true)
public class OrderLine extends BaseEntity {

    @Min(1)
    private final long product;

    @Min(1)
    private final int quantity;

    @DecimalMin("0.00")
    private final BigDecimal subtotal;

    @Builder
    private OrderLine(long id, LocalDateTime createDate, long product, int quantity, BigDecimal subtotal) {
        super(id, createDate);
        this.product = product;
        this.quantity = quantity;
        this.subtotal = subtotal;
    }

}
