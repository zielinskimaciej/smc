package com.mzielinski.smc.model.entities;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;

import static com.mzielinski.smc.datasource.DataSource.getNextSequenceId;
import static java.time.LocalDateTime.now;

@Getter
@EqualsAndHashCode(of = {"id"})
@ToString
public abstract class BaseEntity implements Serializable {

    @Min(1)
    private final long id;

    @NotNull
    private final LocalDateTime createDate;

    protected BaseEntity(long id, LocalDateTime createDate) {
        this.id = id != 0 ? id : getNextSequenceId();
        this.createDate = createDate != null ? createDate : now();
    }

}
