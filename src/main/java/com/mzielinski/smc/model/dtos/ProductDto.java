package com.mzielinski.smc.model.dtos;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import lombok.experimental.Builder;

import java.math.BigDecimal;

@Getter
@EqualsAndHashCode
@ToString
public class ProductDto {

    private final long id;
    private final String name;
    private final BigDecimal price;
    private final int quantity;

    @Builder
    private ProductDto(long id, String name, BigDecimal price, int quantity) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.quantity = quantity;
    }

}
