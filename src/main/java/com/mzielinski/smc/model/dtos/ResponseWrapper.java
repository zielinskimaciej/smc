package com.mzielinski.smc.model.dtos;

import static java.util.Arrays.asList;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class ResponseWrapper<T> {

    private final Status status;
    private final T object;
    private final List<MessageDto> messages;

    private ResponseWrapper(Builder<T> builder) {
        object = builder.object;
        status = builder.status;
        messages = builder.messages.stream()
            .map(message -> new MessageDto(message, status.cssClass))
            .collect(Collectors.toList());
    }

    public enum Status {
        SUCCESS("alert-success"), WARNING("alert-warning"), ERROR("alert-danger");

        private final String cssClass;

        Status(String cssClass) {
            this.cssClass = cssClass;
        }
    }

    public static <T> Builder<T> builder() {
        return new Builder<>();
    }

    public static final class Builder<T> {

        private Status status = Status.SUCCESS;
        private T object;
        private List<String> messages = new ArrayList<>();

        public Builder() {
        }

        public Builder<T> status(Status status) {
            this.status = status;
            return this;
        }

        public Builder<T> object(T object) {
            this.object = object;
            return this;
        }

        public Builder<T> message(String message) {
            this.messages.add(message);
            return this;
        }

        public Builder<T> messages(String... messages) {
            this.messages.addAll(asList(messages));
            return this;
        }

        public ResponseWrapper<T> build() {
            return new ResponseWrapper<>(this);
        }

    }

}
