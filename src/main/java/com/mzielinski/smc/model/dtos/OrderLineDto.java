package com.mzielinski.smc.model.dtos;

import lombok.Getter;
import lombok.ToString;
import lombok.experimental.Builder;

import java.math.BigDecimal;

@Getter
@ToString
public class OrderLineDto {

    private final long product;
    private final int quantity;
    private final BigDecimal subtotal;

    @Builder
    private OrderLineDto(long product, int quantity, BigDecimal subtotal) {
        this.product = product;
        this.quantity = quantity;
        this.subtotal = subtotal;
    }

}
