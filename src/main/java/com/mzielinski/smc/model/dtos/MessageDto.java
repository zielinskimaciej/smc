package com.mzielinski.smc.model.dtos;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class MessageDto {

    private final String cssClass;
    private final String data;

    public MessageDto(String data, String cssClass) {
        this.data = data;
        this.cssClass = cssClass;
    }

}
