package com.mzielinski.smc.model.dtos;

import com.google.common.collect.ImmutableList;
import lombok.Getter;
import lombok.ToString;
import lombok.experimental.Builder;

import java.math.BigDecimal;
import java.util.List;

import static java.util.Collections.emptyList;

@Getter
@ToString
public class OrderDto {

    private final List<OrderLineDto> lines;
    private final BigDecimal totalPrice;

    @Builder
    private OrderDto(List<OrderLineDto> lines, BigDecimal totalPrice) {
        this.lines = new ImmutableList.Builder<OrderLineDto>()
                .addAll(lines != null ? lines : emptyList())
                .build();
        this.totalPrice = totalPrice;
    }

}
