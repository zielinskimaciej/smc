package com.mzielinski.smc.model.converters;

import com.google.common.base.Converter;
import com.mzielinski.smc.model.dtos.ProductDto;
import com.mzielinski.smc.model.entities.Product;
import com.mzielinski.smc.model.entities.WarehouseItem;
import com.mzielinski.smc.repositories.ProductRepository;
import com.mzielinski.smc.repositories.WarehouseRepository;

import javax.annotation.Nonnull;
import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class ProductConverter extends Converter<Product, ProductDto> {

    private final WarehouseRepository warehouseRepository;
    private final ProductRepository productRepository;

    @Inject
    public ProductConverter(WarehouseRepository warehouseRepository, ProductRepository productRepository) {
        this.warehouseRepository = warehouseRepository;
        this.productRepository = productRepository;
    }

    @Override
    protected ProductDto doForward(@Nonnull Product product) {
        final WarehouseItem warehouse = warehouseRepository.findByProduct(product.getId());
        return ProductDto.builder()
                .id(product.getId())
                .name(product.getName())
                .price(product.getPrice())
                .quantity(warehouse.getQuantity())
                .build();
    }

    @Override
    protected Product doBackward(@Nonnull ProductDto productDto) {
        final Product product = productRepository.findOne(productDto.getId());
        return Product.copy(product)
                .name(productDto.getName())
                .price(productDto.getPrice())
                .build();
    }

}
