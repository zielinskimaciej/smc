package com.mzielinski.smc.model.converters;

import com.google.common.base.Converter;
import com.mzielinski.smc.model.dtos.OrderLineDto;
import com.mzielinski.smc.model.entities.OrderLine;
import com.mzielinski.smc.model.entities.Product;
import com.mzielinski.smc.repositories.ProductRepository;

import javax.annotation.Nonnull;
import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class OrderLineConverter extends Converter<OrderLine, OrderLineDto> {

    private final ProductRepository productRepository;

    @Inject
    public OrderLineConverter(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    protected OrderLineDto doForward(@Nonnull OrderLine orderLine) {
        return OrderLineDto.builder()
                .product(orderLine.getProduct())
                .quantity(orderLine.getQuantity())
                .subtotal(orderLine.getSubtotal())
                .build();
    }

    @Override
    protected OrderLine doBackward(@Nonnull OrderLineDto dto) {
        final Product currentProduct = productRepository.findOne(dto.getProduct());
        return OrderLine.builder()
                .product(currentProduct.getId())
                .quantity(dto.getQuantity())
                .subtotal(dto.getSubtotal())
                .build();
    }

}
