function Routs(parent) {

    var self = this;

    self.model = parent;
    self.page = ko.observable("order");

    self.isPage = function(type) {
        return type === self.page();
    };

    self.goToSummary = function () {
        clearMessages();
        if (!self.model.order.isValid()) {
            utils.showInvalidSubmitModal();
        } else {
            self.model.order.updateOrderedLines();
            location.hash = 'summary'
        }
    };

    self.backToHome = function () {
        location.hash = '';
        self.sammy.runRoute('get', '#');
    };

    self.sammy = Sammy(function () {

        this.get('#summary', function () {
            self.page("summary");
        });

        this.get('', function () {
            self.page("order");
        });

    });

    self.sammy.run();

    function clearMessages() {
        self.model.messages(null);
    }

}