function OrderService(parent) {

    var self = this;

    self.buildOrderJson = function() {
        return {
            totalPrice: self.order.grandTotal(),
            lines: $.map(self.order.orderedLines(), function (line) {
                return {
                    product: line.product().id,
                    quantity: line.quantity(),
                    subtotal: line.subtotal()
                };
            })
        }
    };

    self.sendOrder = function() {
        var order = self.buildOrderJson();
        return $.ajax({
            type: "POST",
            url: "order",
            async: false,
            data: JSON.stringify(order)
        }).responseText;
    };

    self.updateViewModel = function(status) {
        self.model.allProducts(retrieveProducts());
        self.products.updateProducts();
        self.order.lines(self.getUpdatedOrderLines(status));
        self.order.updateOrderedLines();
    };

    self.getUpdatedOrderLines = function(status) {
        var orderLines = self.order.createOrderLines();
        if (status != "SUCCESS") {
            self.restorePreviousQuantities(orderLines);
        }
        return orderLines;
    };

    self.restorePreviousQuantities = function(orderLines) {
        $.each(orderLines, function () {
            var orderLine = this;
            var previousOrderLine = $.grep(self.order.orderedLines(), function (orderedLine) {
                return orderedLine.id == orderLine.id;
            })[0];
            if (previousOrderLine != undefined && orderLine.product().quantity > 0) {
                orderLine.quantity(previousOrderLine.quantity());
            }
        });
    };

    self.routToHomePage = function() {
        self.model.routs.backToHome();
    };

    self.showMessage = function(messages) {
        self.model.messages(messages);
    };

    self.buy = function () {
        var result = JSON.parse(self.sendOrder());
        self.updateViewModel(result.status);
        self.routToHomePage();
        self.showMessage(result.messages);
    };

    self.model = parent;
    self.order = parent.order;
    self.products = parent.products;

}