function AppViewModel() {
    var self = this;

    self.allProducts = ko.observable(retrieveProducts());
    self.order = new Order(this);
    self.products = new Products(this, self.order.lines);
    self.routs = new Routs(this);
    self.orderService = new OrderService(this);
    self.messages = ko.observable();
}

$(document).ready(function () {
    var model = new AppViewModel();
    components.registerAll(model);
    ko.applyBindings(model);
});