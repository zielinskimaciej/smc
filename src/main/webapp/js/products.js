function retrieveProducts() {
    var allProducts = $.ajax({type: "GET", url: "products", async: false}).responseText;
    return JSON.parse(allProducts).object
}

function Products(parent, lines) {
    var self = this;

    self.updateProducts = function() {
        var currentProducts = self.createProducts();
        self.products(currentProducts)
    };

    self.createProducts = function() {
        return $.map(self.model.allProducts(), function (product) {
            return new Product(product.id, product.name, product.price, product.quantity, lines);
        });
    };

    self.model = parent;
    self.products = ko.observableArray(self.createProducts());
}

function Product(id, name, price, quantity, lines) {

    var self = this;

    self.id = id;
    self.name = name;
    self.price = price;
    self.quantity = quantity;
    self.lines = lines;
    self.numberOfAvailableItems = ko.pureComputed(function () {
        var orderLinesForSameProduct = self.lines().filter(function (line) {
            return line.product() && line.product().id == self.id;
        });
        var usedOrderLines = countUsedOrderLines(orderLinesForSameProduct);
        return calculateNumberOfAvailableItems(usedOrderLines)
    });

    var countUsedOrderLines = function (lines) {
        var usedOrderLines = 0;
        $.each(lines, function () {
            usedOrderLines += utils.parseToInt(this.quantity());
        });
        return usedOrderLines;
    };

    var calculateNumberOfAvailableItems = function (items) {
        if (items > self.quantity) {
            return 0;
        } else if (items < 0) {
            return self.quantity;
        }
        return self.quantity - items;
    };

}