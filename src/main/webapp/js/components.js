var components = {

    registerProductComponent: function (model) {
        ko.components.register('products-table', {
            viewModel: {instance: model},
            template: {require: 'libs/text!components/products.html'}
        });
    },

    registerOrderComponent: function(model) {
        ko.components.register('order-table', {
            viewModel: {instance: model},
            template: {require: 'libs/text!components/order.html'}
        });
    },

    registerSummaryComponent: function(model) {
        ko.components.register('summary-table', {
            viewModel: {instance: model},
            template: {require: 'libs/text!components/summary.html'}
        });
    },

    registerMessageComponent: function(model) {
        ko.components.register('message', {
            viewModel: {instance: model},
            template: {require: 'libs/text!components/message.html'}
        });
    },

    registerAll: function(model) {
        this.registerProductComponent(model);
        this.registerOrderComponent(model);
        this.registerSummaryComponent(model);
        this.registerMessageComponent(model);
    }

};