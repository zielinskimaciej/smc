function Order(parent) {

    var self = this;

    self.clearLine = function (line) {
        line.quantity(0);
    };

    self.isValid = function () {
        var valid = true;
        $.each(self.lines(), function () {
            if (this.errors().length > 0) {
                this.errors.showAllMessages();
                valid = false;
            }
        });
        return valid;
    };

    self.updateOrderedLines = function() {
        self.orderedLines($.grep(self.lines(), function (line) {
            return utils.parseToInt(line.quantity()) > 0;
        }));
    };

    self.createOrderLines = function() {
        return $.map(self.model.allProducts(), function (product) {
            return new OrderLine(product);
        });
    };

    self.model = parent;
    self.lines = ko.observableArray(self.createOrderLines());
    self.orderedLines = ko.observableArray();
    self.grandTotal = ko.pureComputed(function () {
        var total = 0;
        $.each(self.lines(), function () {
            total += this.subtotal()
        });
        return total;
    });

}

function OrderLine(product) {

    var self = this;

    self.id = product.id;
    self.product = ko.observable(product).extend({required: true});
    self.quantity = ko.observable(0).extend({required: true, min: 0, max: product.quantity});
    self.errors = ko.validation.group(this);
    self.subtotal = ko.pureComputed(function () {
        if (self.product() && self.errors().length == 0) {
            return self.product().price * utils.parseToInt(self.quantity());
        }
        return 0;
    });

}
