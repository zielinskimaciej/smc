var utils = {

    formatCurrency: function (value) {
        return "$" + value.toFixed(2);
    },

    calculatePercent: function (value, max) {
        return ((value / max) * 100).toFixed(0);
    },

    parseToInt: function (value) {
        return parseInt("0" + value, 10)
    },

    retrieveConstant: function (code) {
        return $.ajax({type: "GET", url: "constant/" + code, async: false}).responseText;
    },

    showInvalidSubmitModal: function () {
        BootstrapDialog.alert({
            title: 'WARNING',
            message: this.retrieveConstant('popup.validation.error'),
            type: BootstrapDialog.TYPE_WARNING,
            closable: false
        });
    }

};