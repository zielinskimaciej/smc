#!/bin/bash

if [ "$1" == "tomcat" ]; then
    ./gradlew deployToTomcat
    echo "Running at http://locahost:8080/smc"
elif [ "$1" = "fast" ]
then
    ./gradlew jettyRun
else
    ./gradlew clean test jettyRun
fi